package ru.t1.akolobov.tm;

import ru.t1.akolobov.tm.model.Command;
import ru.t1.akolobov.tm.util.FormatUtil;

import java.util.Scanner;

import static ru.t1.akolobov.tm.constant.ArgumentConst.*;
import static ru.t1.akolobov.tm.constant.TerminalConst.*;

public class Application {

    private static boolean isCommandMode = false;

    public static void main(String[] args) {
        processArguments(args);
        isCommandMode = true;
        displayWelcome();
        porcessCommands();
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        final String arg = args[0];
        processArgument(arg);
        exit();
    }

    private static void porcessCommands() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            System.out.println("ENTER COMMAND:");
            command = scanner.nextLine();
            processCommand(command);
            System.out.println();
        }
    }

    private static void processCommand(final String command) {
        if (command == null || command.isEmpty()) {
            displayError();
            return;
        }
        switch (command) {
            case CMD_VERSION:
                displayVersion();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            case CMD_HELP:
                displayHelp();
                break;
            case CMD_INFO:
                displayInfo();
                break;
            case CMD_EXIT:
                exit();
                break;
            default:
                displayError();
        }
    }

    private static void processArgument(final String arg) {
        switch (arg) {
            case ARG_VERSION:
                displayVersion();
                break;
            case ARG_ABOUT:
                displayAbout();
                break;
            case ARG_HELP:
                displayHelp();
                break;
            case ARG_INFO:
                displayInfo();
                break;
            default:
                displayError();
        }
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.VERSION);
        System.out.println(Command.ABOUT);
        System.out.println(Command.HELP);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.printf("Developer: %s \n", "Alexander Kolobov");
        System.out.printf("e-mail: %s\n", "akolobov@t1-consulting.ru");
    }

    private static void displayInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + FormatUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + FormatUtil.formatBytes(usedMemory));
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayError() {
        System.err.printf("Error! This %s is not supported. \nUse '%s' to display available arguments.\n",
                isCommandMode ? "command" : "argument",
                isCommandMode ? CMD_HELP : ARG_HELP);
    }

}
